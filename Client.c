
#include "Client.h"

Version Client_version =  {0,0,0,"Development Build"};
Version GetClientVersion()
{

	return Client_version;
}

void SendClientVersion()
{
}

void SendServerVersion()
{
}

void CreateConnection()
{
}

void ConnectToServer(int ipaddress)
{
}

void HandleConnectionError()
{
}

void Init_Network()
{
}

void HandleDisconnection()
{
}

void SendData(void* data)
{
}

void OnConnectFailed()
{
}

void OnConnectSucess()
{
}

void OnDisconnection()
{
}
